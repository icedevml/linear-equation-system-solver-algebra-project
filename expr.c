#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "expr.h"

struct expr* make_expr(unsigned int subscript, double value) {
    struct expr* exp = (struct expr*)malloc(sizeof(struct expr));
    exp->subscript = subscript;
    exp->value = value;
    exp->next_expr = 0;
    return exp;
}

void free_expr(struct expr* exp) {
    if (exp->next_expr != 0) {
        free_expr(exp->next_expr);
    }
    free(exp);
}

struct expr* clone_expr(struct expr* exp) {
    struct expr* nw = make_expr(exp->subscript, exp->value);
    if (exp->next_expr != 0) {
        nw->next_expr = clone_expr(exp->next_expr);
    }
    return nw;
};

void add_expr(struct expr* a, struct expr* b) {
    if (a->next_expr != 0) {
        add_expr(a->next_expr, b);
    } else {
        a->next_expr = b;
    }
}

void multiply_expr(struct expr* exp, double scalar) {
    exp->value *= scalar;
    if (exp->next_expr != 0) {
        multiply_expr(exp->next_expr, scalar);
    }
}

void divide_expr(struct expr* exp, double scalar) {
    exp->value /= scalar;
    if (exp->next_expr != 0) {
        divide_expr(exp->next_expr, scalar);
    }
}

struct expr* _find_subscript(struct expr* exp, unsigned int subscript) {
    if (exp->subscript == subscript) {
        return exp;
    }

    if (exp->next_expr != 0) {
        return _find_subscript(exp->next_expr, subscript);
    }

    return NULL; // not found
};

struct expr* normalize_expr(struct expr* exp) {
    struct expr* out = make_expr(exp->subscript, exp->value);

    struct expr* cur = exp;
    struct expr* tmp;
    while(cur->next_expr != 0) {
        cur = cur->next_expr;

        tmp = _find_subscript(out, cur->subscript);
        if (tmp == NULL) {
            add_expr(out, make_expr(cur->subscript, cur->value));
        } else {
            tmp->value += cur->value;
        }
    }

    return out;
};

void print_expr(struct expr* exp, char prefix) {
    printf("%.8lf", exp->value);

    if (exp->subscript != 0) {
        printf("*%c%u", prefix, exp->subscript);
    }

    if (exp->next_expr != 0) {
        if (exp->next_expr->value >= 0) {
            putchar('+');
        }
        print_expr(exp->next_expr, prefix);
    } else {
        putchar('\r');
        putchar('\n');
    }
}
