# README #
This program is capable of solving systems of linear equations in form of augmented matrices. Variable substitutions are carried out symbolically, so it can also provide parametric solutions for the indeterminate system (it means, in case when we have more linearly independent equations than involved variables).
## Usage ##
User has to provide the augmented matrix `(A|B)` in form:

```
(height of A) (width of A)
(A11) (A12) (A13) (A1..) (B1)
(A21) (A22) (A23) (A2..) (B2)
```

## Example ##
**Input:**
```
2 3
1 1 2 2
2 3 7 2
```

**Output:**
```
Input matrix:
    1.00000     1.00000     2.00000 |    2.00000 
    2.00000     3.00000     7.00000 |    2.00000 
    0.00000     0.00000     0.00000 |    0.00000 

Reduced matrix:
    1.00000     1.00000     2.00000 |    2.00000 
    0.00000     1.00000     3.00000 |   -2.00000 
    0.00000     0.00000     0.00000 |    0.00000 

Solution set:
x2 = -2.00000000-3.00000000*p3
x1 = 4.00000000+1.00000000*p3
```