#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "expr.h"
#define max(x, y) ((x) > (y) ? (x) : (y))

double* allocate_array(unsigned int N)
{
    return (double*)calloc(N, sizeof(double));
}

double** allocate_matrix(unsigned int N)
{
    unsigned int i;
    double** M = (double**)malloc((N+1)*sizeof(double*));
    for(i = 1; i <= N; i++)
    {
        M[i] = allocate_array(N+2);
    }
    return M;
}

void read_matrix_xy(unsigned int N, unsigned int X, unsigned int Y, double** M)
{
    unsigned int i;
    unsigned int j;

    for(j = 1; j <= X; j++)
    {
        for(i = 1; i <= Y; i++)
        {
            scanf("%lf", &M[j][i]);
        }
        scanf("%lf", &M[j][N+1]);
    }
}

void read_matrix(unsigned int N, double** M)
{
    read_matrix_xy(N, N, N, M);
}

void print_array(unsigned int N, double* V)
{
    unsigned int i;
    for(i = 1; i <= N; i++)
    {
        printf("v[%i]: %11.5lf\r\n", i, V[i]);
    }
}

void print_matrix(unsigned int N, double** M)
{
    unsigned int i;
    unsigned int j;

    for(j = 1; j <= N; j++)
    {
        for(i = 1; i <= N+1; i++)
        {
            if (i == N+1)
            {
                putchar('|');
            }
            printf("%11.5lf ", M[j][i]);
        }
        putchar('\r');
        putchar('\n');
    }
}

void gauss_reduce(unsigned int N, double** M)
{
    unsigned int i;
    unsigned int j;
    unsigned int k;
    double c;

    for(j=1; j<=N; j++)
    {
        for(i=1; i<=N; i++)
        {
            if(i>j)
            {
                c = M[i][j]/M[j][j];
                for(k=1; k<=N+1; k++)
                {
                    M[i][k] -= c*M[j][k];
                }
            }
        }
    }
}

unsigned int find_first_nonzero(unsigned int N, double* M)
{
    unsigned int i;
    for(i = 1; i <= N; i++)
    {
        if (M[i] != 0 && isfinite(M[i]))
        {
            return i;
        }
    }
    return 0; // not found
}

void solve(unsigned int N, double** M)
{
    unsigned int i;
    unsigned int j;
    unsigned int p;

    struct expr** solution = (struct expr**)calloc(N+1, sizeof(struct expr*));
    int* is_fixed = (int*)calloc(N+1, sizeof(int));

    for(i = N; i > 0; i--)
    {
        p = find_first_nonzero(N, M[i]);
        if (p == 0)
        {
            continue;
        }

        solution[p] = make_expr(0, M[i][N+1]/M[i][p]);
        for(j = p+1; j <= N; j++)
        {
            if (M[i][j] != 0 && isfinite(M[i][j]))
            {
                if (is_fixed[j])
                {
                    struct expr* tmp = clone_expr(solution[j]);
                    multiply_expr(tmp, -M[i][j]);
                    divide_expr(tmp, M[i][p]);
                    add_expr(solution[p], tmp);
                }
                else
                {
                    add_expr(solution[p], make_expr(j, -M[i][j]/M[i][p]));
                }
            }
        }

        is_fixed[p] = 1;
        printf("x%u = ", p);
        struct expr* tmp = solution[p];
        solution[p] = normalize_expr(tmp);
        free_expr(tmp);
        print_expr(solution[p], 'p');
    }
}

int main()
{
    unsigned int X;
    unsigned int Y;
    unsigned int N;
    scanf("%u %u", &X, &Y);
    N = max(X, Y);

    double** M = allocate_matrix(N);

    read_matrix_xy(N, X, Y, M);

    printf("%s", "Input matrix:\r\n");
    print_matrix(N, M);

    printf("%s", "\r\nReduced matrix:\r\n");
    gauss_reduce(N, M);
    print_matrix(N, M);

    printf("%s", "\r\nSolution set:\r\n");
    solve(N, M);
    return 0;
}
