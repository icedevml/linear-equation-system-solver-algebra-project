#pragma once

// structure holding our expression
// (one or more words joined together)
struct expr {
    // if this word contains a variable
    // then here we have it's subscript
    // if not, it should be equal 0
    unsigned int subscript;

    // scalar value of this word
    double value;

    // pointer to next word in this
    // expression or zero if this
    // is the last word
    struct expr* next_expr;
};

// make a single word
struct expr* make_expr(unsigned int subscript, double value);

// free the whole expression
void free_expr(struct expr* exp);

// perform a += b addition on expressions
void add_expr(struct expr* a, struct expr* b);

// print the whole expression
// use 'prefix' as a variable designator
void print_expr(struct expr* exp, char prefix);

// multiply the whole expression by scalar
void multiply_expr(struct expr* exp, double scalar);

// divide the whole expression by scalar
void divide_expr(struct expr* exp, double scalar);

// return a copy of an expression
struct expr* clone_expr(struct expr* exp);

// return a normalized copy of an expression
struct expr* normalize_expr(struct expr* exp);
